#ifndef ALT_UTILS_HPP
#define ALT_UTILS_HPP


#include <map>
#include <cmath>
#include <limits>
#include <sstream>
#include <exception>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <cstring>


class ExceptionStream: public std::exception {
  std::stringstream ss;
  const int extype;
public:
  ExceptionStream(int et = -777) : extype(et) {}
  ExceptionStream(const ExceptionStream &e) : extype(e.type()) {
    ss << e.ss.rdbuf();
  }
  ~ExceptionStream() throw () {
  }
  template<typename T>
  ExceptionStream &operator<<(T t) {
    ss << t;
    return *this;
  }
  const char *what() const throw () {
    // this string must be static or garbage gets printed
    static std::string st;
    st = ss.str();
    return st.c_str();
  }
  int type()const{ return extype; }
};

#define exstream \
ExceptionStream() << "Exception in " << __FILE__ << " at line number " << __LINE__ << " with message: "


template<bool> struct StaticAssert;
template<> struct StaticAssert<true> { };
// Example: StaticAssert<sizeof(int) == 4>();


class Xorshift128{
  typedef unsigned long long int u64;
  u64 st0, st1;
public:
  Xorshift128() : st0(0xffffffffffffffff), st1(0xf0f0f0f0f0f0f0f0) {
	StaticAssert<sizeof(u64) == 8>();
  }
  Xorshift128(const u64 seed) : st0(0xffffffffffffffff), st1(0xf0f0f0f0f0f0f0f0 + seed) {
	StaticAssert<sizeof(u64) == 8>();
  }
  u64 next(){
    u64 s1 = st0;
    const u64 s0 = st1;
    st0 = s0;
    s1 ^= s1 << 23;
    return (st1 = (s1 ^ s0 ^ (s1 >> 17) ^ (s0 >> 26))) + s0;
  }
  // map the u64 range to the lo,hi range
  double dbl_uniform(const double lo, const double hi){
    const double num = next();
    const double den = 0xffffffffffffffff;
    const double ret = lo + double(num) * (hi - lo) / double(den);
    return ret;
  }
  // give more emphasis to points at the extremes of the range
  double dbl_chebyshev(const double lo, const double hi){
    const double pi = 3.1415926535897932385;
    const double in = dbl_uniform(0., pi);
    const double dm2 = std::cos(in);
    const double unit = 0.5 * (1. + dm2);
    const double ret = lo + unit * (hi - lo);
    return ret;
  }
  // modulus, adding 1 to include highest point
  int int_uniform(const int lo, const int hi){
    return lo + next() % (hi + 1 - lo);
  }
};


inline size_t copyStreamToCharArray(char* buf, const size_t size, const std::stringstream& msg){
  std::string st = msg.str(); // safe, but slow copy
  size_t retsize = std::min(st.size(), size);
  std::strncpy(buf, st.c_str(), retsize);
  buf[retsize - 1] = '\0';
  return retsize;
}


struct csv_reader: std::ctype<char> {
	csv_reader() :
			std::ctype<char>(get_table()) {
	}
	static std::ctype_base::mask const* get_table() {
		static std::vector<std::ctype_base::mask> rc(table_size, std::ctype_base::mask());
		rc[','] = std::ctype_base::space; // read *.csv files
		// rc[';'] = std::ctype_base::space; // add other separation characters as desired
		rc['\t'] = std::ctype_base::space;
		rc['\r'] = std::ctype_base::space;
		rc['\n'] = std::ctype_base::space;
		rc[' '] = std::ctype_base::space;
		return &rc[0];
	}
};


class ChannelGroup{
  std::map<std::string, std::vector<double> > vecmap;
  std::string csvName;
public:
  std::string getCsvName()const{ return csvName; }
  std::vector<double>& operator[](const std::string s){
    std::map<std::string, std::vector<double> >::iterator it = vecmap.find(s);
    if(it == vecmap.end()) throw exstream << "Channel name '" << s << "' not found!";
    return it->second;
  }
  const std::vector<double>& operator[](const std::string s)const{
    std::map<std::string, std::vector<double> >::const_iterator it = vecmap.find(s);
    if(it == vecmap.end()) throw exstream << "const Channel name '" << s << "' not found!";
    return it->second;
  }
  ChannelGroup(const std::string cname){
    csvName = cname;
	std::ifstream file(csvName.c_str());
	if(!file.is_open()) throw exstream << "readCsvIntoChannelGroup invalid file: " << csvName;
	file.seekg(0,std::ios::end);
	std::streampos length = file.tellg();
	file.seekg(0,std::ios::beg);
	std::vector<char> buffer(length);
	file.read(&buffer[0], length); // read directly into char buffer
    file.close();
	std::stringstream alldata;
	alldata.imbue(std::locale(std::locale(), new csv_reader()));
	alldata.rdbuf()->pubsetbuf(&buffer[0], length); // set stream to use buffer -- no copy
	std::string header;
	while(true){
		const char e = alldata.get();
		if(e == '\n' || e == '\n') break;
		header.push_back(e);
	}
	std::stringstream ssheader(header);
	ssheader.imbue(std::locale(std::locale(), new csv_reader()));
	std::string item;
	std::vector<std::string> names;
	while(ssheader >> item) names.push_back(item);
	if(names.size() == 0) throw exstream << "readCsvIntoChannelGroup no columns: " << csvName;
	std::vector<double> allnums;
	double num;
    allnums.reserve(length / 4); // assume four characters per number
	while(alldata >> item){
		std::istringstream is(item);
		is >> num;
		allnums.push_back(is.fail() ? std::numeric_limits<double>::signaling_NaN() : num);
	}
	const size_t ncols = names.size();
	const size_t nrows = allnums.size() / names.size();
	std::vector<std::vector<double> *> index;
	for(size_t icol = 0; icol < ncols; ++icol){
      // insert empty vector into vecmap, and store its address in index
      index.push_back(&vecmap[(names[icol])]);
      // resize each vector once
		index[icol]->resize(nrows);
	}
	// row-wise more efficient for allnums
	// column-wise would be more efficient for individual arrays
	for(size_t irow = 0; irow < nrows; ++irow){
		for(size_t icol = 0; icol < ncols; ++icol){
			(*index[icol])[irow] = allnums[irow * ncols + icol];
		}
	}
	std::cerr << "Successfully read '" << csvName << "'   cols " << ncols << "   rows " << nrows << std::endl;
}
};


template<typename Key, typename Val>
Val mapInterp(const std::map<Key, Val>& emap, const Key dist){
  const typename std::map<Key, Val>::const_iterator hi = emap.upper_bound(dist); // first element greater than dist
  if(hi == emap.begin()) return emap.begin()->second; // dist is before beginning
  if(hi == emap.end()) return emap.rbegin()->second; // dist is after end
  typename std::map<Key, Val>::const_iterator lo = hi;
  --lo; // less than or equal to dist
  const Key den = hi->first - lo->first;
  const Key wlo = (hi->first - dist) / den;
  const Key whi = (dist - lo->first) / den;
  return lo->second * wlo + hi->second * whi;
}


template<typename Key, typename Val>
void resample( // assumes all input arrays are sorted by key in ascending order
		std::vector<Val>& outVal,
		const std::vector<Key>& outKey,
		const std::vector<Val>& refVal,
		const std::vector<Key>& refKey){
	if(refKey.size() < 2 || outKey.size() == 0) throw exstream << "resample: too few points " << refKey.size() << ' ' << outKey.size();
	if(refKey.size() != refVal.size()) throw exstream << "resample: ref key and val arrays not same size " << refKey.size() << ' ' << refVal.size();
	outVal.resize(outKey.size());
	size_t iref = 1;
	for(size_t iout = 0; iout < outKey.size(); ++iout){
		if(iout > 0 && !(outKey[iout] >= outKey[iout-1])) throw exstream << "resample: outKey array not sorted";
		// increment ref.high until it is higher than outkey
		// ref.low .......... outkey .....ref.high
		if(outKey[iout] <= refKey.front()){ // outkey is lower than ref.begin
			outVal[iout] = refVal.front(); // use lowest value
			continue;
		}
		while(iref < refKey.size() - 1 && refKey[iref] <= outKey[iout]){
			if(!(refKey[iref] >= refKey[iref-1])) throw exstream << "resample: refKey array not sorted";
			++iref;
		}
		int irefLo = iref;
		while(irefLo >= 0 && refKey[irefLo] >= outKey[iout]) --irefLo;
		if(outKey[iout] >= refKey.back()){ // outkey is higher than ref.end
			outVal[iout] = refVal.back(); // use highest value
			continue;
		}
	    const double den = refKey[iref] - refKey[irefLo];
		const double wlo = (refKey[iref] - outKey[iout]) / den;
	    const double whi = (outKey[iout] - refKey[irefLo]) / den;
	    outVal[iout] = refVal[irefLo] * wlo + refVal[iref] * whi;
	}
}


#endif
